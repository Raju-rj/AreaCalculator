<?php


namespace Pondit\Calculator\AreaCalculator;
class Triangle
{

    protected $base;
    protected $height;

    public function __construct($base, $height)
    {
        $this->base = $base;
        $this->height = $height;
    }

    public function tri()
    {
        $area = $this->base * $this->height / 2;

        return $area;
    }

}