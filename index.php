<?php


require_once "vendor\autoload.php";

use Pondit\Calculator\AreaCalculator\Rectangle;
use Pondit\Calculator\AreaCalculator\Square;
use Pondit\Calculator\AreaCalculator\Circle;
use Pondit\Calculator\AreaCalculator\Triangle;

echo "<h1>Area Calculator</h1><br>";

$rectangle1 = new Rectangle(56,65);
$square = new Square(5, 2);
$circle = new Circle(5,3.1416);
$triangle = new Triangle(20, 10);



echo "<br>Square = " . $square->sqr();
echo "<br>Circle = " . $circle->crl();
echo "<br>Triangle = " . $triangle->tri();

