<?php

namespace Pondit\Calculator\AreaCalculator;
class Square
{

    public $value;
    public $power;

    public function __construct($value, $power)
    {
        $this->value = $value;
        $this->power = $power;
    }

    public function sqr()
    {
        $sqr = $this->value ** $this->power;

        return $sqr;

    }

}