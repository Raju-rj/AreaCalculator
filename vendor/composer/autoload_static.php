<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite125a861a4de6476fa5104ee12e32445
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'Pondit\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Pondit\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite125a861a4de6476fa5104ee12e32445::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite125a861a4de6476fa5104ee12e32445::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
